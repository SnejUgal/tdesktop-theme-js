use crate::TdesktopTheme;
use wasm_bindgen::prelude::*;

pub struct DefaultThemes;

#[wasm_bindgen]
impl DefaultThemes {
    pub fn blue() -> TdesktopTheme {
        TdesktopTheme(tdesktop_theme::default_themes::blue())
    }

    pub fn classic() -> TdesktopTheme {
        TdesktopTheme(tdesktop_theme::default_themes::classic())
    }

    pub fn midnight() -> TdesktopTheme {
        TdesktopTheme(tdesktop_theme::default_themes::midnight())
    }

    pub fn matrix() -> TdesktopTheme {
        TdesktopTheme(tdesktop_theme::default_themes::matrix())
    }
}
