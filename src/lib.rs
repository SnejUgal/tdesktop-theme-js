use js_sys::{Array, Error as JsError, Object, Reflect, Uint8Array};
use tdesktop_theme::{Value, WallpaperExtension::*, WallpaperType::*};
use wasm_bindgen::{prelude::*, JsCast};

pub mod default_themes;

#[wasm_bindgen(typescript_custom_section)]
const COLOR: &'static str = r#"
type ConstructorParameter = undefined | number | Uint8Array;

interface Color {
    red: number;
    green: number;
    blue: number;
    alpha: number;
}

type Value = Color | string | null;

type WallpaperType = `tiled` | `background`;
type WallpaperExtension = `png` | `jpg`;
type WallpaperFileName = `${WallpaperType}.${WallpaperExtension}`;
"#;

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(typescript_type = "ConstructorParameter")]
    pub type ConstructorParameter;

    #[wasm_bindgen(typescript_type = "Color")]
    pub type Color;

    #[wasm_bindgen(method, getter)]
    fn red(this: &Color) -> u8;
    #[wasm_bindgen(method, getter)]
    fn green(this: &Color) -> u8;
    #[wasm_bindgen(method, getter)]
    fn blue(this: &Color) -> u8;
    #[wasm_bindgen(method, getter)]
    fn alpha(this: &Color) -> u8;

    #[wasm_bindgen(typescript_type = "Value")]
    pub type RawValue;

    #[wasm_bindgen(typescript_type = "Color | null")]
    pub type MaybeColor;

    #[wasm_bindgen(typescript_type = "WallpaperType")]
    pub type WallpaperType;
    #[wasm_bindgen(typescript_type = "WallpaperExtension")]
    pub type WallpaperExtension;
    #[wasm_bindgen(typescript_type = "WallpaperFileName")]
    pub type FileName;

    #[wasm_bindgen(typescript_type = "Wallpaper | null")]
    pub type MaybeWallpaper;

    #[wasm_bindgen(typescript_type = "string[]")]
    pub type Variables;
    #[wasm_bindgen(typescript_type = "[string, Color | string][]")]
    pub type Entries;
}

impl<'a> From<&'a Color> for [u8; 4] {
    fn from(color: &'a Color) -> Self {
        [color.red(), color.green(), color.blue(), color.alpha()]
    }
}

impl From<[u8; 4]> for Color {
    fn from([red, green, blue, alpha]: [u8; 4]) -> Self {
        let object = Object::new();
        Reflect::set(&object, &"red".into(), &red.into()).unwrap();
        Reflect::set(&object, &"green".into(), &green.into()).unwrap();
        Reflect::set(&object, &"blue".into(), &blue.into()).unwrap();
        Reflect::set(&object, &"alpha".into(), &alpha.into()).unwrap();
        object.unchecked_into()
    }
}

impl From<Color> for RawValue {
    fn from(color: Color) -> Self {
        color.unchecked_into()
    }
}

impl<'a> From<&'a String> for RawValue {
    fn from(variable: &'a String) -> Self {
        JsValue::from(variable).unchecked_into()
    }
}

impl From<Color> for MaybeColor {
    fn from(color: Color) -> Self {
        color.unchecked_into()
    }
}

/// Represents a `.tdesktop-theme` file structure.
#[wasm_bindgen]
pub struct TdesktopTheme(tdesktop_theme::TdesktopTheme);

/// Represents a theme's wallpaper.
#[wasm_bindgen]
pub struct Wallpaper(tdesktop_theme::Wallpaper);

#[wasm_bindgen]
#[allow(clippy::len_without_is_empty)]
impl TdesktopTheme {
    /// Creates a new `TdesktopTheme`.
    ///
    /// There are several ways to construct a `TdesktopTheme`:
    ///
    /// - `new TdesktopTheme()`, which creates an empty theme;
    /// - `new TdesktopTheme(n: number)`, which also creates an empty theme,
    ///   but pre-allocates memory for `n` variables;
    /// - `new TdesktopThene(contents: Uint8Array)`, which parses a theme
    ///   from the provided array of bytes.
    ///
    /// They correspond to `TdesktopTheme::new`, `TdesktopThene::with_capacity`
    /// and `TdesktopTheme::from_bytes` respectively.
    ///
    /// If a capacity is provided and it can't be turned into a `usize`, or
    /// a `Uint8Array` is provided and the theme cannot be parsed, an error is
    /// thrown.
    ///
    /// # Examples
    ///
    /// ```js
    /// const emptyTheme = new TdesktopTheme();
    /// const preallocatedTheme = new TdesktopTheme(42);
    ///
    /// const stringToBytes = string => Uint8Array.from(
    ///     [...string].map(character => character.codePointAt(0)),
    /// );
    /// const unparsedTheme = stringToBytes(`windowBg: #ffffff`);
    /// const parsedTheme = new TdesktopTheme(unparsedTheme);
    /// ```
    #[wasm_bindgen(constructor)]
    pub fn new(parameter: ConstructorParameter) -> Result<TdesktopTheme, JsValue> {
        let parameter = JsValue::from(parameter);

        if parameter.is_undefined() {
            Ok(Self(tdesktop_theme::TdesktopTheme::new()))
        } else if let Some(n) = parameter.as_f64() {
            if !(usize::MIN as f64..=usize::MAX as f64).contains(&n) || n.fract() != 0.0 {
                return Err(JsError::new("Invalid capacity value").into());
            }

            let n = n as usize;
            Ok(Self(tdesktop_theme::TdesktopTheme::with_capacity(n)))
        } else if let Ok(bytes) = parameter.dyn_into::<Uint8Array>() {
            tdesktop_theme::TdesktopTheme::from_bytes(&bytes.to_vec())
                .map(Self)
                .map_err(|error| string_to_js_error(&error.to_string()))
        } else {
            Err(JsError::new("Invalid constructor argument").into())
        }
    }

    /// Sets `variable`'s value to a color.
    ///
    /// If the provided variable name is invalid or the provided color is
    /// invalid, an error is thrown.
    ///
    /// # Examples
    ///
    /// ```js
    /// const theme = new TdesktopTheme();
    /// theme.setVariable(
    ///     `windowBg`,
    ///     { red: 255, green: 255, blue: 255, alpha: 255 },
    /// );
    /// console.log(theme.getVariable(`windowBg`));
    /// // { red: 255, green: 255, blue: 255, alpha: 255 }
    /// ```
    #[wasm_bindgen(js_name = setVariable)]
    pub fn set_variable(&mut self, variable: String, color: &Color) -> Result<(), JsValue> {
        self.0
            .set_variable(variable, color.into())
            .map_err(string_to_js_error)?;
        Ok(())
    }

    /// Gets the `variable`'s value^
    ///
    /// - If a color is assigned to the variable, a tuple `[number; 4]` is
    ///   returned;
    /// - If the variables is linked to another one, returns a string with
    ///   that variable's name;
    /// - If the variable is not present in the theme, `null` is returned.
    ///
    /// # Examples
    ///
    /// ```js
    /// const theme = new TdesktopTheme();
    /// theme.setVariable(
    ///     `windowBg`,
    ///     { red: 255, green: 255, blue: 255, alpha: 255 },
    /// );
    /// theme.linkVariable(`windowBoldFg`, `windowFg`);
    /// console.log(theme.getVariable(`windowBg`));
    /// // { red: 255, green: 255, blue: 255, alpha: 255 }
    /// console.log(theme.getVariable(`windowBoldFg`)); // "windowFg"
    /// ```
    #[wasm_bindgen(js_name = getVariable)]
    pub fn get_variable(&self, variable: String) -> RawValue {
        match self.0.get_variable(&variable) {
            Some(&Value::Color(color)) => Color::from(color).into(),
            Some(Value::Link(variable)) => variable.into(),
            None => JsValue::NULL.into(),
        }
    }

    /// Checks if the theme contains the `variable`.
    ///
    /// # Examples
    ///
    /// ```js
    /// const theme = new TdesktopTheme();
    /// theme.setVariable(
    ///     `windowBg`,
    ///     { red: 255, green: 255, blue: 255, alpha: 255 },
    /// );
    /// console.log(theme.hasVariable(`windowBg`)); // true
    /// console.log(theme.hasVariable(`windowFg`)); // false
    /// ```
    #[wasm_bindgen(js_name = hasVariable)]
    pub fn has_variable(&self, variable: String) -> bool {
        self.0.has_variable(&variable)
    }

    /// Deletes the `variable` from the theme.
    ///
    /// # Notes
    ///
    /// This method uses `IndexMap::remove` under the hood, which changes the
    /// order of the theme's variables.
    ///
    /// If some variables linked to the deleted one, they're not unlinked.
    ///
    /// # Examples
    ///
    /// ```js
    /// const theme = new TdesktopTheme();
    /// theme.setVariable(
    ///     `windowBg`,
    ///     { red: 255, green: 255, blue: 255, alpha: 255 },
    /// );
    /// theme.deleteVariable(`windowBg`);
    /// console.log(theme.hasVariable(`windowBg`)); // false
    /// ```
    #[wasm_bindgen(js_name = deleteVariable)]
    pub fn delete_variable(&mut self, variable: String) {
        self.0.delete_variable(&variable)
    }

    /// Links the `variable` to `link_to`.
    ///
    /// # Examples
    ///
    /// ```js
    /// const theme = new TdesktopTheme();
    /// theme.linkVariable(`windowBoldFg`, `windowFg`);
    /// console.log(theme.getVariable(`windowBoldFg`)); // "windowFg"
    /// ```
    #[wasm_bindgen(js_name = linkVariable)]
    pub fn link_variable(&mut self, variable: String, link_to: String) -> Result<(), JsValue> {
        self.0
            .link_variable(variable, link_to)
            .map_err(string_to_js_error)
    }

    /// Resolves the `variable` and assigns the final color to the `variable`.
    ///
    /// # Notes
    ///
    /// If the `variable` cannot be resolved, it is deleted.
    ///
    /// # Examples
    ///
    /// ```js
    /// const theme = new TdesktopThene();
    /// theme.setVariable(
    ///     `windowFg`,
    ///     { red: 0, green: 0, blue: 0, alpha: 255 },
    /// );
    /// theme.linkVariable(`windowBoldFg`, `windowFg`);
    /// theme.unlinkVariable(`windowBoldFg`);
    /// console.log(theme.getVariable(`windowBoldFg`));
    /// // { red: 0, green: 0, blue: 0, alpha: 255 }
    /// ```
    #[wasm_bindgen(js_name = unlinkVariable)]
    pub fn unlink_variable(&mut self, variable: String) {
        self.0.unlink_variable(&variable)
    }

    /// Gets the `variable`'s real color. That is, this method resolves
    /// all links until it reaches a color value. If a non-exsistent variable
    /// is met, `null` is returned.
    ///
    /// # Examples
    ///
    /// ```js
    /// const theme = new TdesktopThene();
    /// theme.setVariable(
    ///     `windowFg`,
    ///     { red: 0, green: 0, blue: 0, alpha: 255 },
    /// );
    /// theme.linkVariable(`windowBoldFg`, `windowFg`);
    /// console.log(theme.resolveVariable(`windowBoldFg`));
    /// // { red: 0, green: 0, blue: 0, alpha: 255 }
    /// ```
    #[wasm_bindgen(js_name = resolveVariable)]
    pub fn resolve_variable(&self, variable: String) -> MaybeColor {
        match self.0.resolve_variable(&variable) {
            Some(&color) => Color::from(color).into(),
            None => JsValue::NULL.into(),
        }
    }

    /// Returns the amount of variables in this theme.
    ///
    /// # Examples
    ///
    /// ```js
    /// const theme = new TdesktopTheme();
    /// console.log(theme.variablesAmount); // 0
    /// theme.setVariable(
    ///     `windowBg`,
    ///     { red: 255, green: 255, blue: 255, alpha: 255 },
    /// );
    /// console.log(theme.variablesAmount); // 1
    /// ```
    #[wasm_bindgen(getter, js_name = variablesAmount)]
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns an array of all variable names.
    ///
    /// # Exmaples
    ///
    /// ```js
    /// const theme = new TdesktopTheme();
    /// theme.setVariable(
    ///     `windowBg`,
    ///     { red: 255, green: 255, blue: 255, alpha: 255 },
    /// );
    /// console.log(theme.variables()); // ["windowBg"]
    /// ```
    pub fn variables(&self) -> Variables {
        let array = Array::new();
        for variable in self.0.variables() {
            array.push(&variable.into());
        }
        array.unchecked_into()
    }

    /// Returns an array of variable-value tuples.
    ///
    /// # Exmaples
    ///
    /// ```js
    /// const theme = new TdesktopTheme();
    /// theme.setVariable(
    ///     `windowBg`,
    ///     { red: 255, green: 255, blue: 255, alpha: 255 },
    /// );
    /// console.log(theme.entries());
    /// // [["windowBg", { red: 255, green: 255, blue: 255, alpha: 255 }]]
    /// ```
    pub fn entries(&self) -> Entries {
        let array = Array::new();
        for (variable, value) in &self.0 {
            let variable = variable.into();
            let value = match value {
                &Value::Color(color) => Color::from(color).into(),
                Value::Link(variable) => variable.into(),
            };

            let tuple = Array::of2(&variable, &value).into();
            array.push(&tuple);
        }
        array.unchecked_into()
    }

    /// Serializes the theme into a `.tdesktop-palette` file. That is, only
    /// the variables without the wallpaper.
    #[wasm_bindgen(js_name = toPaletteBytes)]
    pub fn to_palette_bytes(&self) -> Uint8Array {
        Uint8Array::from(self.0.to_palette_bytes().as_slice())
    }

    /// Serialized the theme into a `.tdesktop-theme` file.
    #[wasm_bindgen(js_name = toZipBytes)]
    pub fn to_zip_bytes(&self) -> Result<Uint8Array, JsValue> {
        let zip = self
            .0
            .to_zip_bytes()
            .map_err(|error| string_to_js_error(error.to_string()))?;

        Ok(Uint8Array::from(zip.as_slice()))
    }

    #[wasm_bindgen(getter)]
    pub fn wallpaper(&self) -> MaybeWallpaper {
        self.0
            .wallpaper
            .clone()
            .map_or(JsValue::NULL.into(), |wallpaper| {
                JsValue::from(Wallpaper(wallpaper)).into()
            })
    }

    #[wasm_bindgen(setter)]
    pub fn set_wallpaper(&mut self, wallpaper: MaybeWallpaper) -> Result<(), JsValue> {
        fn invalid_wallpaper() -> JsValue {
            JsError::new("Invalid wallpaper").into()
        }

        let wallpaper = JsValue::from(wallpaper);

        if wallpaper.is_null() {
            self.0.wallpaper = None;
            return Ok(());
        }

        if !wallpaper.is_object() {
            return Err(invalid_wallpaper());
        }

        let kind = if let Some(string) = Reflect::get(&wallpaper, &"type".into())?.as_string() {
            match string.as_str() {
                "tiled" => Tiled,
                "background" => Background,
                _ => return Err(invalid_wallpaper()),
            }
        } else {
            return Err(invalid_wallpaper());
        };

        let extension =
            if let Some(string) = Reflect::get(&wallpaper, &"extension".into())?.as_string() {
                match string.as_str() {
                    "png" => Png,
                    "jpg" => Jpg,
                    _ => return Err(invalid_wallpaper()),
                }
            } else {
                return Err(invalid_wallpaper());
            };

        let bytes = Reflect::get(&wallpaper, &"bytes".into())?
            .dyn_into::<Uint8Array>()
            .map_err(|_| invalid_wallpaper())?
            .to_vec();

        self.0.wallpaper = Some(tdesktop_theme::Wallpaper {
            wallpaper_type: kind,
            extension,
            bytes,
        });

        Ok(())
    }

    #[wasm_bindgen(js_name = fallbackTo)]
    pub fn fallback_to(&self, other: &TdesktopTheme) -> TdesktopTheme {
        Self(&self.0 | &other.0)
    }
}

#[wasm_bindgen]
impl Wallpaper {
    #[wasm_bindgen(getter, js_name = type)]
    pub fn kind(&self) -> WallpaperType {
        match self.0.wallpaper_type {
            Tiled => JsValue::from("tiled").into(),
            Background => JsValue::from("background").into(),
        }
    }

    #[wasm_bindgen(setter, js_name = type)]
    pub fn set_kind(&mut self, new_kind: WallpaperType) -> Result<(), JsValue> {
        self.0.wallpaper_type = match JsValue::from(new_kind).as_string() {
            Some(string) if string == "tiled" => Tiled,
            Some(string) if string == "background" => Background,
            _ => return Err(JsError::new("Invalid wallpaper type value").into()),
        };

        Ok(())
    }

    #[wasm_bindgen(getter)]
    pub fn extension(&self) -> WallpaperExtension {
        match self.0.extension {
            Png => JsValue::from("png").into(),
            Jpg => JsValue::from("jpg").into(),
        }
    }

    #[wasm_bindgen(setter)]
    pub fn set_extension(&mut self, new_kind: WallpaperExtension) -> Result<(), JsValue> {
        self.0.extension = match JsValue::from(new_kind).as_string() {
            Some(string) if string == "png" => Png,
            Some(string) if string == "jpg" => Jpg,
            _ => return Err(JsError::new("Invalid wallpaper extension value").into()),
        };

        Ok(())
    }

    #[wasm_bindgen(getter)]
    pub fn bytes(&self) -> Uint8Array {
        Uint8Array::from(self.0.bytes.as_slice())
    }

    #[wasm_bindgen(setter)]
    pub fn set_bytes(&mut self, bytes: Uint8Array) {
        self.0.bytes = bytes.to_vec();
    }

    #[wasm_bindgen(getter, js_name = fileName)]
    pub fn file_name(&self) -> FileName {
        JsValue::from(self.0.get_filename()).into()
    }
}

fn string_to_js_error<E: AsRef<str>>(error: E) -> JsValue {
    JsError::new(&error.as_ref().to_string()).into()
}
