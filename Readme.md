# `tdesktop-theme-js`

A JavaScript package for parsing and serialization of `.tdesktop-theme`
and `.tdesktop-palette` files. Uses `tdesktop_theme`, a Rust library for working
with Telegram Desktop themes, and compiles to WASM.

## Installing

```bash
npm install tdesktop-theme
```

## Using

### In browser using Webpack

```ts
const { TdesktopTheme } = await import("tdesktop-theme/webpack");
```

Note that WebAssembly has to be fetched asynchronously, so in the import chain
from the entry point to `tdesktop-theme` there needs to be at least one dynamic
import. Only this way does Webpack get the chance to load the required WASM
binary. If you're already using `tdesktop-theme` in a dynamically loaded
module, you can use the `import` statement directly.

### In Node.js without Webpack

```ts
const { TdesktopTheme } = require("tdesktop-theme/node");
```

> Note: if you use Node.js _with_ a bundler, you're supposed to import
> `"tdeskop-theme/webpack"`, but we haven't tested this scenario (yet).

## API

```ts
type ConstructorParameter = undefined | number | Uint8Array;

interface Color {
    red: number;
    green: number;
    blue: number;
    alpha: number;
}

type Value = Color | string | null;

type WallpaperType = `tiled` | `background`;
type WallpaperExtension = `png` | `jpg`;
type WallpaperFileName = `${WallpaperType}.${WallpaperExtension}`;

export class DefaultThemes {
  static blue(): TdesktopTheme;
  static classic(): TdesktopTheme;
  static midnight(): TdesktopTheme;
  static matrix(): TdesktopTheme;
}

export class TdesktopTheme {
  free(): void;
  constructor(parameter: ConstructorParameter);

  setVariable(variable: string, color: Color): void;
  getVariable(variable: string): Value;
  hasVariable(variable: string): boolean;
  deleteVariable(variable: string): void;
  linkVariable(variable: string, link_to: string): void;
  unlinkVariable(variable: string): void;
  resolveVariable(variable: string): Color | null;
  variables(): string[];
  entries(): [string, Color | string][];
  toPaletteBytes(): Uint8Array;
  toZipBytes(): Uint8Array;
  fallbackTo(other: TdesktopTheme): TdesktopTheme;

  readonly variablesAmount: number;
  wallpaper: Wallpaper | null;
}

export class Wallpaper {
  free(): void;

  bytes: Uint8Array;
  extension: WallpaperExtension;
  readonly fileName: WallpaperFileName;
  type: WallpaperType;
}
```

TypeScript declaration files are included in this package. For full description
of these methods, see either the `.d.ts` files or refer to
[`tdesktop_theme`'s documentation] as this package tries to be close to its API.

[`tdesktop_theme`'s documentation]: https://docs.rs/tdesktop_theme

**Attention:** Once you no longer need a `TdesktopTheme`, you must call
its `free` method. Accessing the `wallpaper` property also allocates memory
which should be freed later as well. Not freeing the memory will cause a memory
leak. This is a known problem of JavaScript—WebAssembly interaction and will be
solved when weak references are supported everywhere. 
